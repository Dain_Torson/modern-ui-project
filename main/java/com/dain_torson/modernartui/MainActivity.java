package com.dain_torson.modernartui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.dain_torson.modernartui.view.ModernRectangle;

import java.util.ArrayList;
import java.util.Random;

/*
    Thank you for reviewing my work!

    Please note: According to Android documentation, ActionBarActivity class currently considered as
    deprecated. So, my MainActivity extends more modern AppCompatActivity class, which has no menu
    button on action bar. You can call menu by pressing menu button on your smartphone/emulator.

    Also, be aware that I'm using Java 8 (so should you to launch the project) and minimum required
    Android API level is 18(Android 4.3).

    Finally, all source code is free to use for any purpose, so feel free to modify or reuse it if
    you are interested.

    Best whishes and good luck!

    Ales Veshtort, Belarus, Minsk.
    ales.veshtort@gmail.com

 */



public class MainActivity extends AppCompatActivity {

    private static String URL = "https://www.coursera.org/course/androidpart1";
    private int numOfLeftRect = 2;
    private int numOfRightRect = 3;
    private int colorSeekBarPrevProgress = 0;
    private int seekBarPrevProgress = 0;
    private ArrayList<ModernRectangle> leftRectangles;
    private ArrayList<ModernRectangle> rightRectangles;

    private LinearLayout leftLayout;
    private LinearLayout rightLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        leftLayout = (LinearLayout) findViewById(R.id.leftLayout);
        rightLayout = (LinearLayout) findViewById(R.id.rightLayout);

        leftRectangles = new ArrayList<>();
        rightRectangles = new ArrayList<>();

        for(int idx = 0; idx < numOfLeftRect; ++idx) {
            ModernRectangle rectangle = new ModernRectangle(getApplicationContext(), 1);
            leftRectangles.add(rectangle);
            leftLayout.addView(rectangle);
        }

        for(int idx = 0; idx < numOfRightRect; ++idx) {
            ModernRectangle rectangle = new ModernRectangle(getApplicationContext(), 1);
            rightRectangles.add(rectangle);
            rightLayout.addView(rectangle);
        }

        Random random = new Random();

        int whiteIdx = random.nextInt(numOfLeftRect + numOfRightRect);
        if(whiteIdx < numOfLeftRect) {
            leftRectangles.get(whiteIdx).setWhite(true);
        }
        else {
            rightRectangles.get(whiteIdx - numOfLeftRect).setWhite(true);
        }

        SeekBar colorSeekBar = (SeekBar) findViewById(R.id.colorSeekBar);
        colorSeekBar.setProgress(colorSeekBarPrevProgress);
        colorSeekBar.setOnSeekBarChangeListener(new ColorSeekBarChangeListener());

        SeekBar seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekBar.setProgress(seekBarPrevProgress);
        seekBar.setOnSeekBarChangeListener(new SeekBarChangeListener());

    }

    private void showAboutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.about);
        builder.setMessage(R.string.message);
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.visit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent baseIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(URL));
                Intent chooserIntent = Intent.createChooser(baseIntent, "Start with...");
                startActivity(chooserIntent);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(R.string.later, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.aboutItem:
                showAboutDialog();
                return true;
            case R.id.exitItem:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private int correctnessCheck(int newVal) {

        if(newVal > 255) newVal = newVal - 255;
        if(newVal < 0) newVal = 255 + newVal;

        return newVal;
    }

    private void changeColor(ModernRectangle rectangle, int val) {

        int r = correctnessCheck(Color.red(rectangle.getColor()) + val);
        int g = correctnessCheck(Color.green(rectangle.getColor()) + val);
        int b = correctnessCheck(Color.blue(rectangle.getColor()) - val);

        rectangle.setColor(Color.rgb(r, g, b));
    }

    void addRectangles(LinearLayout target, ArrayList<ModernRectangle> targetList, int num) {

        if(num > 0) {
            for(int i = 0; i < num; ++i) {
                ModernRectangle modernRectangle = new ModernRectangle(getApplicationContext(), 1);
                target.addView(modernRectangle);
                targetList.add(modernRectangle);
            }
        }
        else {
            for(int i = 0; i < Math.abs(num); ++i) {
                ModernRectangle modernRectangle = targetList.remove(targetList.size() - 1);
                target.removeView(modernRectangle);
            }
        }
    }


    private class ColorSeekBarChangeListener implements SeekBar.OnSeekBarChangeListener {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            int diff = progress - colorSeekBarPrevProgress;
            for(ModernRectangle rectangle : leftRectangles) {
                if(!rectangle.isWhite()) {
                    changeColor(rectangle, diff);
                }
            }

            for(ModernRectangle rectangle : rightRectangles) {
                if(!rectangle.isWhite()) {
                    changeColor(rectangle, diff);
                }
            }
            colorSeekBarPrevProgress = progress;
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    }

    private class SeekBarChangeListener implements SeekBar.OnSeekBarChangeListener {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            int diff = progress - seekBarPrevProgress;
            if(Math.abs(diff) < 10) return;

            int toLeft = progress / 10 - leftRectangles.size() + 2;
            int toRight = progress / 10 - rightRectangles.size() + 3;

            addRectangles(leftLayout, leftRectangles, toLeft);
            addRectangles(rightLayout, rightRectangles, toRight);

            seekBarPrevProgress = (progress / 10) * 10;

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    }
}
