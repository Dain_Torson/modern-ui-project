package com.dain_torson.modernartui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.Random;

public class ModernRectangle extends View {

    private Rect rectangle;
    private Paint paint;
    private float weight;
    private int color;
    private boolean white = false;

    public ModernRectangle(Context context, float weight) {
        super(context);
        this.weight = weight;

        Random random = new Random();
        color = Color.rgb(random.nextInt(255), random.nextInt(255), random.nextInt(255));
        if(color == Color.WHITE) white = true;

        rectangle = new Rect();
        rectangle.inset(60, 60);

        paint = new Paint();
        paint.setColor(color);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                0, weight);
        setLayoutParams(params);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        rectangle.set(0, 0, getWidth(), getHeight());

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(color);

        canvas.drawRect(rectangle, paint);

        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(20);
        canvas.drawRect(rectangle, paint);
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
        paint.setColor(color);
        invalidate();
    }

    public boolean isWhite() {
        return white;
    }

    public void setWhite(boolean white) {
        if(white) {
            setColor(Color.WHITE);
        }
        this.white = white;
    }

}
